# R Programming Project
# Author: Mickael Bastard
#
#
# This is the ui logic of a Shiny web application. You can run the
# application by clicking 'Run App' while having the app.R file opened in R studio

library(shiny)

# Define UI for application that plots weather data
myui <- shinyUI(fluidPage(
    
    titlePanel("Meteorological Data across UK"),# Application title
        
    fluidRow(
        column(3,# First column containing input parameters|selector and weather stations map
               wellPanel(
                   uiOutput('selectstations'),# Put adaptive UI component for the stations (will depend on the content of the Sites.csv files)
                   conditionalPanel(
                       condition = "input.tabs == 'plot'",# only show if the plot weather analysis tab is selected
                       selectizeInput("var.selected",
                                      "Select Variable",
                                      selected="Wind Speed",
                                      choices = c("Wind Speed", "Air Temperature", "Relative Humidity", "Visibility")
                                      ),
                       selectizeInput("agg.selected",
                                      "Select Aggregation",
                                      selected="Raw Hourly Data",
                                      choices = c("Raw Hourly Data", "Daily Averages", "Daily Maxima", "Daily Minima", "Monthly Averages", "Monthly Maxima", "Monthly Minima")
                                      ),
                       uiOutput('selectxaxis'),# Put adaptive UI component (choices depend on the selected aggregation)
                       radioButtons('format', 'Document format', c('PDF', 'HTML', 'Word', 'CSV'),
                                    inline = TRUE),
                       downloadButton("downloadReportButton", "Download Report", icon="download")
                       ),
                   
                   conditionalPanel(#The following ui component only shows if the hutton criteria tab is selected
                       condition = "input.tabs == 'huttoncriteria'",
                       dateInput("month.selected",
                             label = "Select Month",
                             #separator = "/",
                             startview = "year",
                             format = "yyyy-mm",
                             value = "2020-05-01",
                             min = "2020-01-01",
                             max = "2020-11-30"
                       ),
                       radioButtons("outputType", "Select output type",
                                    choices=c("Plot", "Calendar", "Table"), selected="Plot")#Report the hutton criteria either in a plot, in a calendar or table
                   ),
               ),
               wellPanel(
                   p("Map"),
                   plotOutput("mapPlot")
               )
        ),
        column(9,#Second larger column to the right containing all the plots and tables in the weather analysis tab and the hutton criteria tab
               wellPanel(
                   tabsetPanel(id = "tabs",
                       tabPanel(title = "Weather Analysis",#Weather analysis tab containing the variable plot and daily mean summary for the last seven days of data available
                                value = "plot",
                                plotOutput("variablePlot"),
                                tags$h4("Daily Mean Summary of Weather Variables"),
                                dataTableOutput("summaryVariables")
                       ),
                       tabPanel(title = "Hutton Criteria",#Hutton criteria tab reporting the results of the analysis to alert of the risk of potato blight
                                value = "huttoncriteria",
                                p("The Hutton Criteria is used for potato late blight risk analysis."),
                                p("It occurs in a particular day when the following criteria are met: "),
                                tags$ol(
                                    tags$li("The two previous days have a minimum temperature of at least 10 °C"), 
                                    tags$li("The two previous days have at least six hours of relative humidity of 90% or higher")
                                ),
                                uiOutput("ui.hutton") # Put adaptive UI to report hutton criteria (table, plot or calendar)
                       )
                   )
               )
               )
        
    ),
        
))
